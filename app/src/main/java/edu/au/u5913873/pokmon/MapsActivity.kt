package edu.au.u5913873.pokmon

import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Build
import android.support.v4.app.FragmentActivity
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.util.Log
import android.widget.Toast

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions

class MapsActivity : FragmentActivity(), OnMapReadyCallback {

    private var mMap: GoogleMap? = null
    private var userPower: Double = 0.0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        checkPermission()
        loadPokemon()
        myThread().start()

        var userSimulateLocation = Location("start")
        userSimulateLocation.latitude = 13.612917
        userSimulateLocation.longitude = 100.836787

        myLocation = userSimulateLocation

        var simulateThread = Simulate()
        simulateThread.start()
    }

    val ACCESSLOCATION = 101
    fun checkPermission() {
        if(Build.VERSION.SDK_INT >= 23) {
            if(ActivityCompat.checkSelfPermission(this,android.Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),ACCESSLOCATION)

            }
        }
        getUserLocation()
    }

    fun getUserLocation() {
        Toast.makeText(this, "User location access is ON", Toast.LENGTH_LONG).show()

        var myLocationListener = MyLocationListener()
        var locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 15, 3f, myLocationListener)

        var myThread = myThread()
        myThread.start()

    }

    var pokemonsList = ArrayList<Pokemon>()


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            ACCESSLOCATION -> {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getUserLocation()
                } else {
                    Toast.makeText(this, "We cannot access to your location", Toast.LENGTH_LONG).show()
                }
            }
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    var myLocation:Location? = null
    inner class MyLocationListener: LocationListener {
        constructor() {
            myLocation = Location("")
            myLocation!!.latitude = 0.0
            myLocation!!.longitude = 0.0
        }

        override fun onLocationChanged(location: Location) {
            myLocation = location
//            Toast.makeText(applicationContext, "${location!!.latitude}, ${location!!.longitude}", Toast.LENGTH_LONG)
        }

        override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {
        }

        override fun onProviderEnabled(provider: String?) {
        }

        override fun onProviderDisabled(provider: String?) {
        }

    }

    var oldLocation:Location? = null

    inner class Simulate : Thread {
        constructor():super()

        override fun run() {
            Thread.sleep(5000)
            for (i in 0..pokemonsList.size - 1) {
                var pok = pokemonsList.get(i)
                var location: Location? = pok.location

                while (myLocation != location && pok.isCatch!! == false) {
                    if (myLocation!!.latitude < location!!.latitude) {
                        myLocation!!.latitude += 0.000001
                    } else {
                        myLocation!!.latitude -= 0.000001
                    }

                    if (myLocation!!.longitude < location!!.longitude) {
                        myLocation!!.longitude += 0.000001
                    } else {
                        myLocation!!.longitude -= 0.000001
                    }
                    Thread.sleep(3)
                }
                Thread.sleep(3000)
            }
        }
    }





    inner class myThread: Thread {

        constructor() :super() {
            oldLocation = Location("")
            oldLocation!!.latitude = 0.0
            oldLocation!!.longitude = 0.0
        }

        override fun run() {
            while (true) {
                try {
//                    if(oldLocation!!.distanceTo(myLocation) == 0f) {
//                        continue
//                    }
                    runOnUiThread {
                        mMap!!.clear()

                        val latLng = LatLng(myLocation!!.latitude, myLocation!!.longitude)
                        mMap!!.addMarker(MarkerOptions().position(latLng).title("ME").snippet("here is my location")
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.boy)))
                        mMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng,18f))
//                        for (j in 0..pokemonList.size-1) {
//                            myLocation!!.latitude = pokemonList[j].location!!.latitude
//                            myLocation!!.longitude = pokemonList[j].location!!.longitude

                        for (i in 0..pokemonsList.size - 1) {
                            var newPokemon = pokemonsList[i]
                            if (newPokemon.isCatch == false) {
                                var pokemonlocation = LatLng(newPokemon.location!!.latitude, newPokemon.location!!.longitude)


                                mMap!!.addMarker(MarkerOptions()
                                        .position(pokemonlocation).title(newPokemon.name).snippet(newPokemon.description)
                                        .icon(BitmapDescriptorFactory.fromResource(newPokemon.image!!)))


                                if (myLocation!!.distanceTo(newPokemon.location!!) < 2) {
                                    newPokemon.isCatch = true;
                                    pokemonsList[i] = newPokemon
                                    userPower += newPokemon.power!!
                                    Toast.makeText(applicationContext, "You catch new pokemon,your new power is $userPower",
                                            Toast.LENGTH_LONG).show()
                                }
                            }
                        }
//                        }



                    }
                } catch (ex: Exception) {
                    Log.e("THREAD-RUN", ex.message)
                }
                Thread.sleep(1000)
            }
            super.run()
        }
    }



    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

    }

    fun loadPokemon() {
        pokemonsList.add(Pokemon("Charmander", "Charmander is a Fire type Pokémon introduced in Generation 1. It is known as the Lizard Pokémon",
                R.drawable.charmander, 13.613061, 100.835827, false, 65))
        pokemonsList.add(Pokemon("Pikachu", "Pikachu is an Electric type Pokémon introduced in Generation 1. It is known as the Mouse Pokémon.",
                R.drawable.pikachu, 13.613431, 100.835795, false, 82))
        pokemonsList.add(Pokemon("Squirtle", "Squirtle is a Water type Pokémon introduced in Generation 1. It is known as the Tiny Turtle Pokémon.",
                R.drawable.squirtle, 13.612017, 100.837805, false, 66))
        pokemonsList.add(Pokemon("Zubat", "Zubat is a Poison/Flying type Pokémon introduced in Generation 1. It is known as the Bat Pokémon.",
                R.drawable.zubat, 13.612372, 100.835659, false, 54))
    }

}