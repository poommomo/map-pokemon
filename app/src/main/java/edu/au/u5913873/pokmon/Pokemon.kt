package edu.au.u5913873.pokmon

/**
 * Created by Momo on 9/19/2017 AD.
 */

import android.location.Location

class Pokemon {
    var name:String? = null
    var description:String? = null
    var image:Int? = null
    var location:Location? = Location(name)
    var isCatch:Boolean? = null
    var power:Int? = null

    constructor(name: String?, description: String?, image: Int?, latitude: Double?, longitude: Double?, isCatch: Boolean?, power: Int?) {
        this.name = name
        this.description = description
        this.image = image
        this.location = location
        this.location!!.latitude = latitude!!
        this.location!!.longitude = longitude!!
        this.isCatch = isCatch
        this.power = power
    }
}